package test.cdi;

import javax.enterprise.inject.Produces;

public class AbcProducer {
	
	@Produces
	public A createA(B b) {
		return new A(new A.Options(), b);
	}
	
	@Produces
	public B createB(C c) {
		return new B(new B.Options(), c);
	}
	
	@Produces
	public C createC() {
		return new C(new C.Options());
	}
}
