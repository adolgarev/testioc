package test.cdi;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Weld weld = new Weld();
		WeldContainer container = weld.initialize();
		A a = container.instance().select(A.class).get();
		a.apply();
		weld.shutdown();
	}
}
