package test.cdi;

public class B {
	
	private Options options;
	
	private C c;
	
	public static class Options {
	}

	public B(Options options, C c) {
		this.options = options;
		this.c = c;
	}
	
	public void apply() {
		c.apply();
		System.out.println(options);
	}
}
