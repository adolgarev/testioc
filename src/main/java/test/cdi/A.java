package test.cdi;

public class A {
	
	private Options options;
	
	private B b;
	
	public static class Options {
	}

	public A(Options options, B b) {
		this.options = options;
		this.b = b;
	}
	
	public void apply() {
		b.apply();
		System.out.println(options);
	}
}
