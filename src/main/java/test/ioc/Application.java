package test.ioc;

public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		C c = new C(new C.Options());
		B b = new B(new B.Options(), c);
		A a = new A(new A.Options(), b);
		a.apply();
	}
}
