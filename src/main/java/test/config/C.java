package test.config;

public class C {
	
	private Options options;
	
	public static class Options {
	}

	public C(AbcConfiguration conf) {
		this.options = conf.getOptionsForC();
	}
	
	public void apply() {
		System.out.println(options);
	}
}
