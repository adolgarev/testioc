package test.config;

public class AbcConfiguration {

	public A.Options getOptionsForA() {
		return new A.Options();
	}
	
	public B.Options getOptionsForB() {
		return new B.Options();
	}
	
	public C.Options getOptionsForC() {
		return new C.Options();
	}

}

