package test.config;

public class B {
	
	private Options options;
	
	private C c;
	
	public static class Options {
	}

	public B(AbcConfiguration conf) {
		this.options = conf.getOptionsForB();
		this.c = new C(conf);
	}
	
	public void apply() {
		c.apply();
		System.out.println(options);
	}
}
