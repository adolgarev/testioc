package test.config;

public class A {
	
	private Options options;
	
	private B b;
	
	public static class Options {
	}

	public A(AbcConfiguration conf) {
		this.options = conf.getOptionsForA();
		this.b = new B(conf);
	}
	
	public void apply() {
		b.apply();
		System.out.println(options);
	}
}
