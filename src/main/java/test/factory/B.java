package test.factory;

public class B {
	
	private Options options;
	
	private C c;
	
	public static class Options {
	}

	public B(Options options, AbcFactory factory) {
		this.options = options;
		this.c = factory.getC();
	}
	
	public void apply() {
		c.apply();
		System.out.println(options);
	}
}
