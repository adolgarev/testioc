package test.factory;

public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AbcFactory factory = new AbcFactory();
		A a = factory.getA();
		a.apply();
	}
}
