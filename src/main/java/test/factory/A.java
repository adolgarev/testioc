package test.factory;

public class A {
	
	private Options options;
	
	private B b;
	
	public static class Options {
	}

	public A(Options options, AbcFactory factory) {
		this.options = options;
		this.b = factory.getB();
	}
	
	public void apply() {
		b.apply();
		System.out.println(options);
	}
}
