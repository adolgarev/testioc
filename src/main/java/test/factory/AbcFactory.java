package test.factory;

public class AbcFactory {
	
	public A getA() {
		return new A(new A.Options(), this);
	}
	
	public B getB() {
		return new B(new B.Options(), this);
	}
	
	public C getC() {
		return new C(new C.Options(), this);
	}
}
