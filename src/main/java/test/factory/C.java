package test.factory;

public class C {
	
	private Options options;
	
	public static class Options {
	}

	public C(Options options, AbcFactory factory) {
		this.options = options;
	}
	
	public void apply() {
		System.out.println(options);
	}
}
