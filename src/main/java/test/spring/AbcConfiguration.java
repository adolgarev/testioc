package test.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class AbcConfiguration {

	@Bean
	public A.Options getOptionsForA() {
		return new A.Options();
	}
	
	@Bean
	public B.Options getOptionsForB() {
		return new B.Options();
	}
	
	@Bean
	public C.Options getOptionsForC() {
		return new C.Options();
	}

}
