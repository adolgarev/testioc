package test.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class A {
	
	@Autowired
	private Options options;
	
	@Autowired
	private B b;
	
	public static class Options {
	}
	
	public A() {
	}
	
	public void apply() {
		b.apply();
		System.out.println(options);
	}
}
