package test.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class C {
	
	@Autowired
	private Options options;
	
	public static class Options {
	}
	
	public C() {
	}
	
	public void apply() {
		System.out.println(options);
	}
}
