package test.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class B {
	
	@Autowired
	private Options options;
	
	@Autowired
	private C c;
	
	public static class Options {
	}
	
	public B() {
	}
	
	public void apply() {
		c.apply();
		System.out.println(options);
	}
}
