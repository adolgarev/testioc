package test.naive;

public class B {
	
	private Options options;
	
	private C c;
	
	public static class Options {
	}

	public B(Options options, C.Options optionsForC) {
		this.options = options;
		this.c = new C(optionsForC);
	}
	
	public void apply() {
		c.apply();
		System.out.println(options);
	}
}
