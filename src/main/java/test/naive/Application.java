package test.naive;

public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		A a = new A(new A.Options(), new B.Options(), new C.Options());
		a.apply();
	}

}
