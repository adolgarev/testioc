package test.naive;

public class C {
	
	private Options options;
	
	public static class Options {
	}

	public C(Options options) {
		this.options = options;
	}
	
	public void apply() {
		System.out.println(options);
	}
}
