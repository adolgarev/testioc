package test.naive;

public class A {
	
	private Options options;
	
	private B b;
	
	public static class Options {
	}

	public A(Options options, B.Options optionsForB, C.Options optionsForC) {
		this.options = options;
		this.b = new B(optionsForB, optionsForC);
	}
	
	public void apply() {
		b.apply();
		System.out.println(options);
	}
}
